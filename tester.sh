#!/bin/bash

if [ $# -lt 1 ]
then
    echo -e "[ERREUR] $0 : pas assez d'arguments"
    echo -e "USAGE : $0 exercice [-c]"
    exit 2
fi

exercise=ex_$1
binary=$exercise.bin
output="output.test"

if [ ! -e $exercise ]
then
    echo "KO: exercise [$binary] not found"
    exit 2
fi

if [[ $# -eq 2 ]] && [[ $2 -eq "-c" ]]
then
    cd tests/$exercise && cc *.c -o $binary && cd - &> /dev/null
    ./tests/$exercise/$binary &> "./tests/${exercise}_REF"
else
#    cp ./$exercise/* ./tests/$exercise/.
#    cc ./tests/$exercise/*.c -o ./$binary
#    sudo -u student ./$binary &> output.test
#    chown student:student ./$exercise/moulinette.c 
    sudo -u student cc ./$exercise/*.c ./tests/$exercise/moulinette.c -o $binary
    #chmod +x $binary
    sudo -u student ./$binary &> output.test
    res=$(diff -a <(cat -e "./tests/${exercise}_REF" ) <(cat -e output.test))

    if [ $? -ne 0 ]; then
	echo "KO: check your traces below this line..."
	echo "$res"
    else
	echo "OK"
    fi
fi
